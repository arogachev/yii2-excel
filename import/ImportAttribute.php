<?php

namespace arogachev\excel\import;

use yii\base\Object;

class ImportAttribute extends Object
{
    public $name;

    /**
     * @var ImportCell
     */
    public $valueCell;

    public $value;

    public $isPkLink = false;
}
