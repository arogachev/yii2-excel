<?php

namespace arogachev\excel\import;

class Exception extends \yii\base\Exception
{
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Import Exception';
    }
}
