<?php

namespace arogachev\excel\import;

use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * @author Alexey Rogachev <arogachev90@gmail.com>
 */
class ImportRow extends Object
{
    public $attributes = [];

    public $modelClass;

    /**
     * @var \yii\db\ActiveRecord
     */
    public $model;

    public $pkLink;

    public $rowLabel;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->model = new $this->modelClass;
    }

    public function loadUpdatedModel()
    {
        $pk = array_intersect_key($this->getAttributeValues(), $this->model->getPrimaryKey(true));

        // Model doesn't have primary key
        if (!$pk) {
            return;
        }

        // Single primary key
        if (count($pk) == 1 && !reset($pk)) {
            return;
        }

        // Composite primary key
        foreach ($pk as $value) {
            if (!$value) {
                $message = "Для редактируемой модели в строке $this->rowLabel ";
                $message .= 'указаны не все составляющие составного первичного ключа.';

                throw new Exception($message);
            }
        }

        /**
         * @var $modelClass \yii\db\ActiveRecord
         */
        $modelClass = $this->modelClass;
        $model = $modelClass::findOne($pk);

        if (!$model && count($pk) == 1) {
            throw new Exception("Модель для редактирования в строке $this->rowLabel не найдена.");
        }

        if (count($pk) == 1) {
            $this->model = $model;

            foreach ($pk as $attribute => $value) {
                unset($this->attributes[$attribute]);
            }
        }
    }

    public function getAttributeValues()
    {
        return ArrayHelper::getColumn($this->attributes, 'value');
    }
}
