<?php

namespace arogachev\excel\import\importers;

use arogachev\excel\import\Exception;
use yii\helpers\ArrayHelper;

/**
 * @author Alexey Rogachev <arogachev90@gmail.com>
 */
class Importer
{
    /**
     * @param array $config
     * @return AdvancedImporter|BasicImporter
     * @throws Exception
     */
    public static function getInstance($config)
    {
        if (ArrayHelper::keyExists('model', $config)) {
            return new BasicImporter($config);
        }

        if (ArrayHelper::keyExists('models', $config)) {
            return new AdvancedImporter($config);
        }

        throw new Exception('Не указаны модели для импорта.');
    }
}
