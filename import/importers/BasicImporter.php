<?php

namespace arogachev\excel\import\importers;

use Yii;
use arogachev\excel\import\ImportModel;
use arogachev\excel\import\ImportRow;
use yii\base\Exception;

/**
 * @author Alexey Rogachev <arogachev90@gmail.com>
 */
class BasicImporter extends BaseImporter
{
    /**
     * @var string The model for import - full class name should be provided
     */
    public $model;

    protected $_importModel;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_importModel = new ImportModel($this->model);
    }

    /**
     * @inheritdoc
     */
    public function fillImportRows()
    {
        $c = 0;

        foreach ($this->getRawArray() as $rowLabel => $row) {
            $c++;

            // Column names should be placed in the first row
            /* @var $attributeNames array */
            if ($c == 1) {
                $attributeNames = $this->getAttributeNames($row, $rowLabel, $this->model);
                if (!$attributeNames) {
                    throw new Exception("Не найдены атрибуты в строке $rowLabel.");
                }

                continue;
            }

            $attributeValues = [];
            $attributeColumnLabels = [];

            foreach ($row as $columnLabel => $data) {
                $attributeName = $attributeNames[$columnLabel];
                $attributeValues[$attributeName] = $data;
                $attributeColumnLabels[$attributeName] = $columnLabel;
            }

            $this->convertAttributeValues($attributeValues, $rowLabel, $attributeColumnLabels);

            $this->_importRows[] = new ImportRow([
                'modelClass' => $this->model,
                'attributeValues' => $attributeValues,
                'attributeColumnLabels' => $attributeColumnLabels,
                'rowLabel' => $rowLabel,
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    protected function safeRun()
    {
        parent::safeRun();

        foreach ($this->_importRows as $importRow) {
            $importRow->loadUpdatedModel();
            $importRow->model->setAttributes($importRow->attributeValues);

            if (!$importRow->model->validate()) {
                $this->_wrongModel = $importRow;

                throw new Exception("Данные в модели в строке $importRow->rowLabel неверны.");
            }
        }

        Yii::$app->db->transaction(function() {
            foreach ($this->_importRows as $importRow) {
                $importRow->model->save(false);
            }
        });
    }

    protected function getImportModel()
    {
        return $this->_importModel;
    }
}
