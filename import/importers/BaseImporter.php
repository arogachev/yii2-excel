<?php

namespace arogachev\excel\import\importers;

use PHPExcel_IOFactory;
use arogachev\excel\import\Exception;
use arogachev\excel\import\ImportAttribute;
use arogachev\excel\import\ImportModel;
use arogachev\excel\import\ImportRow;
use yii\base\InvalidParamException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * @author Alexey Rogachev <arogachev90@gmail.com>
 *
 * @property ImportModel $importModel
 * @property null|\yii\db\ActiveRecord $wrongModel
 */
abstract class BaseImporter extends Object
{
    /**
     * @var null|string Error message in case of failed import
     */
    public $error;

    /**
     * @var null|string Path where Excel import file is located
     */
    public $filePath;

    /**
     * @var int Sheets where imported data is located
     */
    public $sheets = '*';

    /**
     * @var ImportRow[] Models for import
     */
    protected $_importRows = [];

    /**
     * @var \PHPExcel_Worksheet
     */
    protected $_sheet;

    /**
     * @var null|\yii\db\ActiveRecord The model where import failed
     */
    protected $_wrongModel;

    /**
     * @var \PHPExcel
     */
    protected $_phpExcel;


    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     */
    public function init()
    {
        if (!$this->filePath) {
            throw new InvalidParamException('Не загружен файл или не указан путь к файлу.');
        }

        if (!file_exists($this->filePath)) {
            throw new InvalidParamException("Файла по указанному пути '$this->filePath' не существует.");
        }
    }

    public function run()
    {
        try {
            $this->safeRun();
        } catch (Exception $e) {
            $this->error = $e->getMessage();

            return false;
        }

        return true;
    }

    protected function safeRun()
    {
        $this->_phpExcel = PHPExcel_IOFactory::load($this->filePath);

        foreach ($this->getSheetsIndexes($this->_phpExcel) as $index) {
            $this->_sheet = $this->_phpExcel->getSheet($index);
            $this->fillImportRows($this->_sheet->getRowIterator());
        }

        if (!$this->_importRows) {
            throw new Exception('Нет данных для импорта.');
        }
    }

    /**
     * @param \PHPExcel$phpExcel
     * @return array|int
     * @throws Exception
     */
    protected function getSheetsIndexes($phpExcel)
    {
        if (is_array($this->sheets)) {
            return $this->sheets;
        }

        if ($this->sheets === '*') {
            $sheetCount = $phpExcel->getSheetCount();
            if (!$sheetCount) {
                throw new Exception('В импортируемом файле нет ни одного листа.');
            }

            return range(0, $sheetCount - 1);
        }

        return [$this->sheets];
    }

    /**
     * Get model where import failed
     * This model can be used for getting validation errors etc.
     * @return null|\yii\db\ActiveRecord
     */
    public function getWrongModel()
    {
        return $this->_wrongModel;
    }

    /**
     * Fill import rows for import from raw array
     * @param \PHPExcel_Worksheet_RowIterator|\PHPExcel_Worksheet_Row[] $rows
     */
    abstract protected function fillImportRows($rows);

    /**
     * Get attribute names from row
     * @param \PHPExcel_Worksheet_Row $row Excel row
     * @return array Attribute names indexed with column labels
     * @throws Exception
     */
    protected function getAttributeNames($row)
    {
        $model = $this->importModel->model;
        $attributeNames = [];

        foreach ($row->getCellIterator() as $cell) {
            if (!$cell->getValue()) {
                if (!$attributeNames) {
                    $this->throwCellException('Не указан атрибут.', $cell);
                } else {
                    break;
                }
            }

            $attributeNames[$cell->getColumn()] = $this->getAttributeName($cell);
        }

        $pk = $model->primaryKey();
        $importPk = array_intersect($pk, $model->attributes());

        // In case of composite primary keys all PK attributes should be specified
        if ($importPk && count($pk) != count($importPk)) {
            $message = "В строке {$row->getRowIndex()} указаны не все атрибуты ";
            $message .= 'составного первичного ключа для редактирования модели.';

            throw new Exception($message);
        }

        return $attributeNames;
    }

    /**
     * Get attribute name from cell
     * @param \PHPExcel_Cell $cell
     * @return string Attribute name
     * @throws \yii\base\Exception
     */
    protected function getAttributeName($cell)
    {
        if (!$this->importModel->useAttributeLabels) {
            $attributeName = $cell->getValue();
        } else {
            $attributeName = ArrayHelper::getValue($this->importModel->labelAttributes, $cell->getValue());

            if (!$attributeName) {
                $this->throwCellException("Не указан атрибут для лейбла.", $cell);
            }
        }

        return $attributeName;
    }

    /**
     * @param ImportAttribute[] $attributes
     * @throws Exception
     */
    protected function convertAttributeValues(&$attributes)
    {
        foreach ($attributes as $attributeName => $attribute) {
            $importModelAttribute = $this->importModel->importModelAttributes[$attribute->name];

            if (!$attribute->isPkLink && $importModelAttribute->labelValue) {
                $query = call_user_func($importModelAttribute->labelValue, $attribute->value);
                $models = $query->all();
                if (count($models) != 1) {
                    $this->throwCellException('Не удалось сопоставить лейбл со значением.', $attribute->valueCell->instance);
                }

                $attribute->value = $models[0]->{$importModelAttribute->labelValueAttributeName};
            } elseif ($importModelAttribute->valueLabels) {
                $value = $importModelAttribute->getValueByLabel($attribute->value);
                if (!$value) {
                    $this->throwCellException('Не удалось сопоставить лейбл со значением.', $attribute->valueCell->instance);
                }

                $attribute->value = $value;
            }
        }
    }

    /**
     * Generate exception related with invalid data in cell
     * @param string $message Text message of exception
     * @param \PHPExcel_Cell $cell
     * @throws Exception
     */
    protected function throwCellException($message, $cell)
    {
        $sheetTitle = $cell->getWorksheet()->getTitle();
        $cellMessage = "При подготовке данных для импорта возникла ошибка в листе \"$sheetTitle\" в ячейке ";
        $cellMessage .= $cell->getCoordinate() . ".\n";

        throw new Exception($cellMessage . $message);
    }

    abstract protected function getImportModel();
}
