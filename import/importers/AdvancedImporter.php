<?php

namespace arogachev\excel\import\importers;

use arogachev\excel\import\ImportCell;
use PHPExcel_Cell;
use Yii;
use arogachev\excel\import\ImportAttribute;
use arogachev\excel\import\Exception;
use arogachev\excel\import\ImportModel;
use arogachev\excel\import\ImportRow;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

/**
 * @author Alexey Rogachev <arogachev90@gmail.com>
 */
class AdvancedImporter extends BaseImporter
{
    const MODE_DEFAULT_ATTRIBUTES = 'defaultAttributes';

    const MODE_IMPORT = 'import';

    public $blockDetection;

    /**
     * @var array Models for import
     */
    public $models;

    protected $_currentMode;

    protected $_currentModelClass;

    /**
     * @var ImportModel[]
     */
    protected $_importModels = [];

    protected $_lastPk;

    /**
     * @var array Relations between model labels and full model class names
     */
    protected $_modelLabelsMap = [];

    protected $_pkLinks = [];

    /**
     * @var string
     */
    protected $_rememberedRowLink;

    /**
     * @var array
     */
    protected $_rememberedRowsLinks = [];


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        foreach ($this->models as $config) {
            $importModel = new ImportModel($config);
            $this->_importModels[$importModel->class] = $importModel;
        }

        $this->fillModelLabelsMap();
        $this->initBlockDetection();
    }

    public function initBlockDetection()
    {
        $defaults = [
            'modelLabel' => function ($cell) {
                /* @var $cell PHPExcel_Cell */
                return $cell->getStyle()->getFont()->getBold();
            },
            'defaultsModelLabel' => function ($cell) {
                /* @var $cell PHPExcel_Cell */
                $isBold = $cell->getStyle()->getFont()->getBold();
                $isUnderline = $cell->getStyle()->getFont()->getUnderline();

                return ($isBold && $isUnderline != 'none');
            },
            'attributeName' => function ($cell) {
                /* @var $cell PHPExcel_Cell */
                return $cell->getStyle()->getFont()->getItalic();
            },
            'rememberedPkLink' => function ($cell) {
                /* @var $cell PHPExcel_Cell */
                $startColor = $cell->getStyle()->getFill()->getStartColor()->getRGB();
                $endColor = $cell->getStyle()->getFill()->getEndColor()->getRGB();

                return ($startColor == '00B0F0' && in_array($endColor, ['00B0F0', '33CCCC']));
            },
            'receivedPkLink' => function ($cell) {
                /* @var $cell PHPExcel_Cell */
                $startColor = $cell->getStyle()->getFill()->getStartColor()->getRGB();
                $endColor = $cell->getStyle()->getFill()->getEndColor()->getRGB();
                $yellowColor = 'FFFF00';

                return ($startColor == $yellowColor && $endColor == $yellowColor);
            },
            'rememberedRowLink' => function ($cell) {
                /* @var $cell PHPExcel_Cell */
                $startColor = $cell->getStyle()->getFill()->getStartColor()->getRGB();
                $endColor = $cell->getStyle()->getFill()->getEndColor()->getRGB();
                $greenColor = '00FF00';

                return ($startColor == $greenColor && $endColor == $greenColor);
            },
            'receivedRowLink' => function ($cell) {
                /* @var $cell PHPExcel_Cell */
                $startColor = $cell->getStyle()->getFill()->getStartColor()->getRGB();
                $endColor = $cell->getStyle()->getFill()->getEndColor()->getRGB();
                $orangeColor = 'F1C232';

                return ($startColor == $orangeColor && $endColor == $orangeColor);
            },
        ];

        foreach ($defaults as $blockName => $function) {
            if (!isset($this->blockDetection[$blockName])) {
                $this->blockDetection[$blockName] = $function;
            }
        }
    }

    /**
     * @throws InvalidParamException
     */
    protected function fillModelLabelsMap()
    {
        foreach ($this->_importModels as $importModel) {
            foreach ($importModel->labels as $label) {
                if (isset($this->_modelLabelsMap[$label])) {
                    throw new InvalidParamException('В modelLabels все значения должны быть уникальны.');
                } else {
                    $this->_modelLabelsMap[$label] = $importModel->class;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function fillImportRows($rows)
    {
        foreach ($rows as $row) {
            // Skipping completely empty rows
            // Only received pk links can be empty

            $currentCell = $row->getCellIterator()->current();

            if ($this->getReceivedPkLink($currentCell) === null && $currentCell->getValue() === null) {
                continue;
            }

            if (!$row->getCellIterator()->valid()) {
                continue;
            }

            if ($this->getReceivedRowLink($currentCell)) {
                $this->fillImportRows($this->_rememberedRowsLinks[$this->getReceivedRowLink($currentCell)]);

                continue;
            }

            if ($this->getRememberedRowLink($currentCell) !== null) {
                $this->_rememberedRowLink = $this->_rememberedRowLink ? null : $this->getRememberedRowLink($currentCell);

                continue;
            }

            if ($this->_rememberedRowLink) {
                $this->_rememberedRowsLinks[$this->_rememberedRowLink][] = clone $row;

                continue;
            }

            if ($this->parseDefaultsModelLabel($row)) {
                $this->_currentMode = self::MODE_DEFAULT_ATTRIBUTES;

                continue;
            }

            if ($this->parseModelLabel($row)) {
                $this->_currentMode = self::MODE_IMPORT;

                continue;
            }

            if ($this->parseAttributeNames($row)) {
                continue;
            }

            if ($this->_currentMode == self::MODE_IMPORT) {
                $this->parseAttributeValues($row);
            } elseif ($this->_currentMode == self::MODE_DEFAULT_ATTRIBUTES) {
                $this->parseDefaultAttributeValues($row);
            }
        }
    }

    protected function safeRun()
    {
        parent::safeRun();

        Yii::$app->db->transaction(function() {
            foreach ($this->_importRows as $index => $importRow) {
                foreach ($importRow->attributes as $attributeName => $attribute) {
                    if ($attribute->isPkLink) {
                        $value = $attribute->value;
                        $pk = ($value === '') ? $this->_lastPk : ArrayHelper::getValue($this->_pkLinks, $value);
                        if (!$pk) {
                            throw new Exception('Не удается сопоставить первичный ключ.');
                        }

                        $attribute->value = $pk;
                    }
                }

                $importRow->loadUpdatedModel();

                $importRow->model->setAttributes(ArrayHelper::getColumn($importRow->attributes, 'value'), false);
                if (!$importRow->model->save()) {
                    $this->_wrongModel = $importRow;

                    throw new Exception("Данные в модели в строке $importRow->rowLabel неверны.");
                }

                $nextImportRow = ArrayHelper::getValue($this->_importRows, $index + 1);
                if ($nextImportRow && $importRow->modelClass != $nextImportRow->modelClass) {
                    if (!is_array($importRow->model->primaryKey) && $importRow->model->primaryKey) {
                        $this->_lastPk = $importRow->model->primaryKey;
                    }
                }

                if ($importRow->pkLink) {
                    if (!is_array($importRow->model->primaryKey) && $importRow->model->primaryKey) {
                        $this->_pkLinks[$importRow->pkLink] = $importRow->model->primaryKey;
                    }
                }
            }
        });
    }

    /**
     * @param \PHPExcel_Cell $cell
     * @return bool
     * @throws Exception
     */
    protected function parseModelClass($cell)
    {
        $value = $cell->getValue();

        if ($value === null) {
            $this->throwCellException('Не заполнен лейбл модели.', $cell);
        }

        $modelClass = ArrayHelper::getValue($this->_modelLabelsMap, $cell->getValue());
        if (!$modelClass) {
            $this->throwCellException('Не удалось определить модель по лейблу.', $cell);
        }

        $this->_currentModelClass = $modelClass;

        return true;
    }

    /**
     * @param \PHPExcel_Worksheet_Row $row
     * @return bool
     */
    protected function parseModelLabel($row)
    {
        $cell = $row->getCellIterator()->current();
        if (!call_user_func($this->blockDetection['modelLabel'], $cell)) {
            return false;
        }

        return $this->parseModelClass($cell);
    }

    /**
     * @param \PHPExcel_Worksheet_Row $row
     * @return bool
     */
    protected function parseDefaultsModelLabel($row)
    {
        $cell = $row->getCellIterator()->current();
        if (!call_user_func($this->blockDetection['defaultsModelLabel'], $cell)) {
            return false;
        }

        return $this->parseModelClass($cell);
    }

    /**
     * @param \PHPExcel_Worksheet_Row $row
     * @return bool
     * @throws Exception
     */
    protected function parseAttributeNames($row)
    {
        $cell = $row->getCellIterator()->current();
        if (!call_user_func($this->blockDetection['attributeName'], $cell)) {
            return false;
        }

        if (!$this->_currentModelClass) {
            $message = "В строке {$row->getRowIndex()} заданию значений атрибутов ";
            $message .= 'должно предшествовать указание лейбла модели.';

            throw new Exception($message);
        }

        $this->importModel->setCurrentAttributeNames($this->getAttributeNames($row));

        return true;
    }

    /**
     * @param \PHPExcel_Worksheet_Row $row
     */
    protected function parseAttributeValues($row)
    {
        $attributes = [];
        $allowedColumns = array_keys($this->importModel->currentAttributeNames);

        foreach ($row->getCellIterator() as $cell) {
            if (!in_array($cell->getColumn(), $allowedColumns)) {
                continue;
            }

            $attributeName = $this->importModel->currentAttributeNames[$cell->getColumn()];

            $attribute = new ImportAttribute;
            $attribute->name = $attributeName;

            $pkLink = $this->getReceivedPkLink($cell);
            if ($pkLink === null) {
                $attribute->value = $cell->getCalculatedValue();
            } else {
                $attribute->value = $pkLink;
                $attribute->isPkLink = true;
            }

            $attribute->valueCell = new ImportCell([
                'phpExcel' => $this->_phpExcel,
                'sheetCodeName' => $this->_sheet->getCodeName(),
                'coordinate' => $cell->getCoordinate(),
            ]);
            $attributes[$attributeName] = $attribute;
        }

        $this->mergeDefaultAttributeValues($attributes);
        $this->convertAttributeValues($attributes);

        $config = [
            'modelClass' => $this->_currentModelClass,
            'attributes' => $attributes,
            'rowLabel' => $row->getRowIndex(),
        ];

        // Parse remembered pk link

        $currentAttributeNames = $this->importModel->currentAttributeNames;
        end($currentAttributeNames);
        $columnLabel = key($currentAttributeNames);

        $rememberedPkLink = null;

        $cell = $this->_sheet->getCell(++$columnLabel . $row->getRowIndex());

        $rememberedPkLink = $this->getRememberedPkLink($cell);
        if ($rememberedPkLink !== null) {
            $config['pkLink'] = $rememberedPkLink;
        }

        $this->_importRows[] = new ImportRow($config);
    }

    /**
     * @param ImportAttribute[] $attributes
     */
    protected function mergeDefaultAttributeValues(&$attributes)
    {
        foreach ($this->importModel->importModelAttributes as $importModelAttribute) {
            if (!$importModelAttribute->_defaultValueCell) {
                continue;
            }

            $cell = $importModelAttribute->_defaultValueCell->instance;

            $attribute = ArrayHelper::getValue($attributes, $importModelAttribute->name);
            if ($attribute && $attribute->value === null) {
                $receivedPkLink = $this->getReceivedPkLink($cell);
                if ($receivedPkLink !== null) {
                    $attribute->isPkLink = true;
                    $attribute->value = $receivedPkLink;
                } else {
                    $attribute->value = $cell->getCalculatedValue();
                }
            } else {
                $config = [
                    'name' => $importModelAttribute->name,
                    'value' => $cell->getCalculatedValue(),
                    'valueCell' => new ImportCell([
                        'phpExcel' => $this->_phpExcel,
                        'sheetCodeName' => $this->_sheet->getCodeName(),
                        'coordinate' => $cell->getCoordinate(),
                    ]),
                ];

                $receivedPkLink = $this->getReceivedPkLink($cell);
                if ($receivedPkLink !== null) {
                    $config['value'] = $receivedPkLink;
                    $config['isPkLink'] = true;
                }

                $attributes[$importModelAttribute->name] = new ImportAttribute($config);
            }
        }
    }

    /**
     * @param \PHPExcel_Worksheet_Row $row
     * @throws \PHPExcel_Exception
     */
    protected function parseDefaultAttributeValues($row)
    {
        foreach ($this->importModel->currentAttributeNames as $columnLabel => $attribute) {
            $cell = $this->_sheet->getCell($columnLabel . $row->getRowIndex());
            $this->importModel->importModelAttributes[$attribute]->setDefaultValueCell(new ImportCell([
                'phpExcel' => $this->_phpExcel,
                'sheetCodeName' => $this->_sheet->getCodeName(),
                'coordinate' => $cell->getCoordinate(),
            ]));
        }
    }

    /**
     * @param PHPExcel_Cell $cell
     * @return mixed
     */
    protected function getRememberedPkLink($cell)
    {
        if (!call_user_func($this->blockDetection['rememberedPkLink'], $cell)) {
            return null;
        }

        $value = $cell->getCalculatedValue();
        if (!$value) {
            $this->throwCellException('Не заполнен запоминаемый первичный ключ.', $cell);
        }

        return $value;
    }

    /**
     * @param PHPExcel_Cell $cell
     * @return mixed
     */
    protected function getReceivedPkLink($cell)
    {
        if (!call_user_func($this->blockDetection['receivedPkLink'], $cell)) {
            return null;
        }

        return (string) $cell->getCalculatedValue();
    }

    /**
     * @param PHPExcel_Cell $cell
     * @return mixed
     */
    protected function getRememberedRowLink($cell)
    {
        if (!call_user_func($this->blockDetection['rememberedRowLink'], $cell)) {
            return null;
        }

        $value = $cell->getCalculatedValue();
        if (!$value) {
            $this->throwCellException('Не заполнено название для запоминаемой строки.', $cell);
        }

        return (string) $cell->getCalculatedValue();
    }

    /**
     * @param PHPExcel_Cell $cell
     * @return mixed
     */
    protected function getReceivedRowLink($cell)
    {
        if (!call_user_func($this->blockDetection['receivedRowLink'], $cell)) {
            return null;
        }

        return (string) $cell->getCalculatedValue();
    }

    /**
     * @return null|ImportModel
     */
    protected function getImportModel()
    {
        return ArrayHelper::getValue($this->_importModels, $this->_currentModelClass);
    }
}
