<?php

namespace arogachev\excel\import;

use yii\base\Object;

/**
 * @property \PHPExcel_Cell $instance
 */
class ImportCell extends Object
{
    /**
     * @var \PHPExcel
     */
    public $phpExcel;

    /**
     * @var integer
     */
    public $sheetCodeName;

    /**
     * @var string
     */
    public $coordinate;

    /**
     * @return \PHPExcel_Cell
     * @throws \PHPExcel_Exception
     */
    public function getInstance()
    {
        return $this->phpExcel->getSheetByCodeName($this->sheetCodeName)->getCell($this->coordinate);
    }
}
