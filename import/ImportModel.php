<?php

namespace arogachev\excel\import;

use yii\base\Event;
use yii\base\InvalidParamException;
use yii\base\Object;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @author Alexey Rogachev <arogachev90@gmail.com>
 *
 * @property array $currentAttributeNames
 * @property ImportModelAttribute[] $importModelAttributes
 * @property array $labelAttributes
 * @property ActiveRecord $model
 */
class ImportModel extends Object
{
    /**
     * @var array
     */
    public $attributes = [];

    /**
     * @var string Full model class with namespace
     */
    public $class;

    /**
     * @var boolean Extend specified attributes with allowed attributes from the model
     */
    public $extendAttributes = true;

    /**
     * @var array Labels used to determine model
     * Usage of two labels is recommended - for single and plural word forms
     * For example: ['Product', 'Products']
     */
    public $labels = [];

    /**
     * @var bool Use attribute labels for attribute names (more user-friendly)
     */
    public $useAttributeLabels = true;

    /**
     * @var string
     */
    public $scenario;

    protected $_currentAttributeNames = [];

    /**
     * @var ImportModelAttribute[]
     */
    protected $_importModelAttributes = [];

    /**
     * @var array All label attributes for faster access
     */
    protected $_labelAttributes = [];

    /**
     * @var ActiveRecord
     */
    protected $_model;

    public function init()
    {
        $this->validateModel();

        $events = [
            ActiveRecord::EVENT_INIT,
            ActiveRecord::EVENT_AFTER_FIND,
        ];

        foreach ($events as $event) {
            Event::on($this->class, $event, function ($event) {
                if ($this->scenario) {
                    $event->sender->scenario = $this->scenario;
                }
            });
        }

        $this->_model = new $this->class;

        $this->validateAttributeLabels();

        foreach ($this->attributes as $config) {
            $this->_importModelAttributes[] = $this->initImportModelAttribute($config);
        }

        if ($this->extendAttributes) {
            $this->extendImportModelAttributes();
        }

        $importModelAttributes = $this->_importModelAttributes;
        $this->_importModelAttributes = [];
        foreach ($importModelAttributes as $importModelAttribute) {
            $this->_importModelAttributes[$importModelAttribute->name] = $importModelAttribute;
        }

        $this->_labelAttributes = ArrayHelper::map($this->_importModelAttributes, 'label', 'name');
        $this->validateLabelAttributes();
    }

    /**
     * @return ActiveRecord
     */
    protected function getModel()
    {
        return $this->_model;
    }

    public function getLabelAttributes()
    {
        return $this->_labelAttributes;
    }

    public function getImportModelAttributes()
    {
        return $this->_importModelAttributes;
    }

    /**
     * Validate model
     * @throws InvalidParamException
     */
    protected function validateModel()
    {
        $object = new $this->class;
        if (!($object instanceof ActiveRecord)) {
            throw new InvalidParamException("Модель $this->class не унаследована от \\yii\\db\\ActiveRecord.");
        }
    }

    /**
     * Validate attribute labels
     * @throws InvalidParamException
     */
    protected function validateAttributeLabels()
    {
        if (!$this->useAttributeLabels) {
            return;
        }

        if (!$this->model->attributeLabels()) {
            $message = "Для модели '$this->class' названия лейблов для атрибутов не заданы.";

            throw new InvalidParamException($message);
        }
    }

    protected function validateLabelAttributes()
    {
        if ($this->_labelAttributes != array_unique($this->_labelAttributes)) {
            $message = "Для модели '$this->class' названия лейблов для атрибутов не уникальны.";

            throw new InvalidParamException($message);
        }
    }

    protected function extendImportModelAttributes()
    {
        $existingAttributes = ArrayHelper::getColumn($this->_importModelAttributes, 'name');
        $missingAttributes = array_diff($this->getAllowedModelAttributes(), $existingAttributes);

        foreach ($missingAttributes as $attributeName) {
            $this->_importModelAttributes[] = $this->initImportModelAttribute([
                'name' => $attributeName,
            ]);
        }
    }

    protected function initImportModelAttribute($config)
    {
        $config['importModel'] = $this;

        return new ImportModelAttribute($config);
    }

    protected function getAllowedModelAttributes()
    {
        $attributes = [];
        $model = $this->model;

        foreach ($model->attributes() as $attribute) {
            if (in_array($attribute, $model->primaryKey()) || $model->isAttributeSafe($attribute)) {
                $attributes[] = $attribute;
            }
        }

        return $attributes;
    }

    public function getCurrentAttributeNames()
    {
        return $this->_currentAttributeNames;
    }

    public function setCurrentAttributeNames($value)
    {
        $this->_currentAttributeNames = $value;
    }
}
