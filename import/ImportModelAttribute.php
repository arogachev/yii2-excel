<?php

namespace arogachev\excel\import;

use PHPExcel_Cell;
use yii\base\InvalidParamException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * @author Alexey Rogachev <arogachev90@gmail.com>
 */
class ImportModelAttribute extends Object
{
    /**
     * @var ImportModel
     */
    public $importModel;

    /**
     * @var string Label
     */
    public $label;

    /**
     * @var callable Compliance of label and value
     * For example: function ($label) {
     *     return User::find()->select('id')->where(['name' => $label]);
     * }
     */
    public $labelValue;

    public $labelValueAttributeName;

    /**
     * @var string Name
     */
    public $name;

    /**
     * @var array Compliance of values and labels
     * For example: [1 => 'Football', 2 => 'Basketball']
     */
    public $valueLabels = [];

    /**
     * @var ImportCell
     */
    public $_defaultValueCell;

    /**
     * @var array Flipped valueLabels for faster access
     */
    protected $_labelValues = [];

    /**
     * @var array Count of each label in valueLabels
     * Used for informing user about ambiguity of compliance label and value
     */
    protected $_valueLabelsCount = [];

    public function init()
    {
        $this->validateName();

        if ($this->importModel->extendAttributes && !$this->label) {
            $this->label = $this->importModel->model->getAttributeLabel($this->name);
        }

        $this->validateLabel();

        if ($this->valueLabels) {
            $this->_valueLabelsCount = array_count_values($this->valueLabels);
            $this->_labelValues = array_flip($this->valueLabels);
        }
    }

    /**
     * @return ImportCell
     */
    public function getDefaultValueCell()
    {
        return $this->_defaultValueCell;
    }

    /**
     * @param ImportCell $value
     */
    public function setDefaultValueCell($value)
    {
        $this->_defaultValueCell = $value;
    }

    public function getValueByLabel($label)
    {
        if (ArrayHelper::getValue($this->_valueLabelsCount, $label) != 1) {
            return null;
        }

        return $this->_labelValues[$label];
    }

    public function validateName()
    {
        $importModel = $this->importModel;
        $model = $this->importModel->model;

        if (!$this->name) {
            throw new InvalidParamException("Для атрибута в модели '{$importModel->class}' не указано название.");
        }

        if (!in_array($this->name, $importModel->model->attributes())) {
            $message = "Атрибут '$this->name' для модели '{$importModel->class}' не существует.";

            throw new InvalidParamException($message);
        }

        if (in_array($this->name, $model->primaryKey())) {
            return;
        }

        if (in_array($this->name, ArrayHelper::getColumn($this->importModel->attributes, 'name'))) {
            return;
        }

        if (!$model->isAttributeSafe($this->name)) {
            $message = "Атрибут '$this->name' для модели '{$importModel->class}' не разрешен для импорта.";

            throw new InvalidParamException($message);
        }
    }

    public function validateLabel()
    {
        if (!$this->importModel->useAttributeLabels) {
            return;
        }

        if (!$this->label) {
            $message = "Для атрибута '$this->name' модели '{$this->importModel->class}' не задан лейбл.";

            throw new InvalidParamException($message);
        }
    }
}
